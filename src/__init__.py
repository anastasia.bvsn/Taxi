from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask('__name__')
app.config["SECRET_KEY"] = "null00000000"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = 'postgresql://anastasiia:@localhost:5432/applications'
app.static_folder = 'static'
db = SQLAlchemy(app)

from src.website.models import *
from src.website.views import *

db.create_all()
