from flask import Flask
from flask_wtf import FlaskForm
from wtforms import Form, StringField, PasswordField, IntegerField
from wtforms.validators import InputRequired, Length

class LoginForm(Form):
    username = StringField('', validators=[InputRequired(message=u'Please fill in username form'), Length(min=4, max=20)], render_kw={"placeholder": "Username"})
    password = PasswordField('', validators=[InputRequired(message=u'Please fill in password form'), Length(min=4, max=20)], render_kw={"placeholder": "Password"})

class ApplicationForm(Form):
    name = StringField('', validators=[InputRequired(message=u'What is your name?'), Length(min=3, max=20)], render_kw={"placeholder": "Your name"})
    phone = IntegerField('', validators=[InputRequired(message=u'Please enter your phone number')], render_kw={"placeholder": "Phone number"})
    adress = StringField('', validators=[InputRequired(message=u'Where do you want to go?'), Length(min=4, max=30)], render_kw={"placeholder": "Destination"})
