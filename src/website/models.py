from datetime import datetime

from src import db


class Application(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    phone = db.Column(db.String(15))
    adress = db.Column(db.String(100))
    date = db.Column(db.DateTime)

    def __init__(self, name, phone, adress):
        self.name = name
        self.phone = phone
        self.adress = adress
        self.date = datetime.utcnow()
