from flask import Flask, render_template, redirect, url_for, request, session
from src import app
from src.website.models import Application
from src.website.lib.auth import *
from src.website.lib.wtf_forms import LoginForm, ApplicationForm
# from bl import *

@app.route('/', methods=['GET'])
def main_page():
    form = ApplicationForm()
    return render_template('main.html', form=form)

@app.route('/', methods=['POST'])
def add_application():
    form = ApplicationForm(request.form)
    if not form.validate():
        error = "Please fill in all forms correctly and then submit the order"
        return render_template('main.html', error=error, form=form)
    else:
        name = request.form['name']
        phone = request.form['phone']
        adress = request.form['adress']

        application = Application(name=name, phone=phone, adress=adress)

        db.session.add(application)
        db.session.commit()

        return render_template('success.html')


@app.route('/login', methods=['GET'])
def login_page():
    form = LoginForm()
    log_out()
    return render_template('login.html', form=form)

@app.route('/login', methods=['POST'])
def check_if_registered():
    form = LoginForm(request.form)
    if not is_form_validated():
        error = "Please fill forms in correctly"
        return render_template('login.html', error=error, form=form)
    if check_auth(form.username.data, form.password.data):
        session['user'] = form['username']
        return redirect(url_for('admin_page'))
    else:
        error = "User is not found"
        return render_template('login.html', error=error, form=form)


@app.route('/admin', methods=['GET'])
def admin_page():
    if not user_request():
        return redirect(url_for ('login_page'))

    return render_template('admin.html', items=Application.query.all())
